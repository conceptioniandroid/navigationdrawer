package com.conceptioni.navigationdrawer;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView nav_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawerLayout);
        nav_view = findViewById(R.id.nav_view);

        nav_view.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }

        int id = menuItem.getItemId();

        switch (id) {
            case R.id.nav_item1:
                Toast.makeText(this, "item1 clicked", Toast.LENGTH_SHORT).show();

            case R.id.nav_item2:
                Toast.makeText(this, "item2 clicked", Toast.LENGTH_SHORT).show();

            case R.id.nav_item3:
                Toast.makeText(this, "item3 clicked", Toast.LENGTH_SHORT).show();

            case R.id.nav_item4:
                Toast.makeText(this, "item4 clicked", Toast.LENGTH_SHORT).show();


        }

        return false;
    }

    public void openCloseDrawer(View v) {
        {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        }
    }
}
