package com.conceptioni.navigationdrawer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by 123 on 21-02-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextViewLight extends TextView {
    public TextViewLight(Context context) {
        super(context);
        init();
    }

    public TextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/POPPINS-LIGHT_0.TTF");
        setTypeface(tf);
    }
}
